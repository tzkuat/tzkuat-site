+++
title = "Publications"
slug = "publications"
+++

List of the different publications produced
============================================

Thesis: Cryptography in the electronic payment system
---------------------------------------------------------------------

* [Thesis ODT format [FR]](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-M%c3%a9moire.odt)
* [Thesis PDF format [FR]](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-M%c3%a9moire.pdf)
* [Annexes ODT format [FR]](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-Annexes.odt)
* [Annexes PDF format [FR]](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-Annexes.pdf)


Blog and others
-------------------------

* [Net-Security.fr Blog](https://net-security.fr)
* [Installing PFSense cluster with Clément Olier [FR]](https://repo.tzku.at/school_rendered/security/MickaelRigonnaux_SecuriteSI_Part_3.pdf)
* [PFSense installation with Clément Olier [FR]](https://repo.tzku.at/school_rendered/security/MickaelRigonnaux_SecuriteSI_Part_2.pdf)
* [ARP Spoofin Attack [FR]](https://repo.tzku.at/school_rendered/security/Rapport%20TP%20Protocole%20ARP%20Olier-Rigonnaux.pdf)
* [Pentest Web [FR]](https://repo.tzku.at/school_rendered/security/AttaquesApplicatives_Rigonnaux_Olier.pdf)

Presentations
-------------

* [OSINT presentation ODP format [FR]](https://repo.tzku.at/presentation/OSINT-tzkuat.odp)
* [OSINT presentation PDF format [FR]](https://repo.tzku.at/presentation/OSINT-tzkuat.pdf)
* [Tor presentation with Fabio Pace ODP format [FR]](https://repo.tzku.at/presentation/TheOnionReport.odp_1.odp)
* [Tor presentation with Fabio Pace PDF format [FR]](https://repo.tzku.at/presentation/TheOnionReport.pdf)
* [Cryptography presentation ODP format [FR]](https://repo.tzku.at/presentation/CRYPTO-tzkuat.odp)
* [Cryptography presentation PDF format [FR]](https://repo.tzku.at/presentation/CRYPTO-tzkuat.pdf)
