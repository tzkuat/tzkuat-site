+++
title = "A Propos"
description = "A propos de Mickael Rigonnaux"
date = "2024-03-30"
aliases = ["a-propos"]
author = "Mickael Rigonnaux"
+++

## Qui suis-je ?

@tzkuat : Ingénieur en sécurité SI / RSSI, libriste et éternel touche à tout. Je suis passionné par l'informatique plus généralement et surtout : les systèmes GNU/Linux, le réseau, la sécurité, l'OSINT,  la cryptographie et la protection de la vie privée. Vous trouverez sur ce site mes pubications et mes contributions : mémoire de d'études, billets de blog, présentation, etc. ainsi que plusieurs moyens de me contacter ou de me suivre.

Je passe pas mal de temps à essayer et bricoler des choses sur des systèmes GNU/Linux : Homelab, Arch, Pop!OS, etc.

Dans tous les cas, n'hésitez pas à échanger avec moi sur Twitter/Mastodon ou à me suivre !

## Parcours pro

Je suis diplomé d'un BTS SIO SISR et deux titres RNCP (équivalent B+3 & B+5) dans les systèmes, les réseaux et la sécurité. Depuis le début de ma carrière j'évolue dans des environnements ou la sécurité est la priorité : PCI-DSS, ISO27001 & HDS.

Après 3 ans dans une société spécialisée dans les paiements j'évolue aujourd'hui dans le milieu du datacenter mais toujours avec une "casquette" Sécurité SI.

Certification : ISO27005 Risk Manager, ISO27001 Lead Auditor & Lead Implementer, EBIOS Risk Manager PECB, Stormshield CSNA


