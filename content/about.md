+++
title = "About"
description = "About Mickael Rigonnaux"
date = "2024-03-30"
aliases = ["about-us"]
author = "Mickael Rigonnaux"
+++


## Who am I ?

IT security engineer / CISO, free software lover and eternal dabbler. I am passionate about computer science in general and especially: GNU/Linux systems, networking, security, OSINT, cryptography and privacy. You will find on this site my publications and contributions: thesis, blog posts, presentation, etc. as well as several ways to contact or follow me.

I spend a lot of time trying and tinkering with GNU/Linux systems: Homelab, Arch, Pop!OS, etc.

In any case, feel free to chat with me on Twitter/Mastodon or follow me!

## Career path

I have a BTS SIO SISR and two RNCP titles (B+3 & B+5 equivalent) in systems, networks and security. Since the beginning of my career I have been working in environments where security is the priority: PCI-DSS, ISO27001 & HDS.

After 3 years in a company specialised in payments, I am now working in the datacenter environment but still with an IT security "hat".

Certification : ISO27005 Risk Manager, ISO27001 Lead Auditor & Lead Implementer, EBIOS Risk Manager PECB, Stormshield CSNA